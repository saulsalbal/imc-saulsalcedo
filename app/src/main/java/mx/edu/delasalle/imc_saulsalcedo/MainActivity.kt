package mx.edu.delasalle.imc_saulsalcedo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Context
import android.icu.text.DecimalFormat
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnCalc.setOnClickListener {


            if(txtPeso.text.toString() != "" && txtAltura.text.toString() != "" ) {
                val imc = calcIMC(txtPeso.text.toString(), txtAltura.text.toString())
                val df = DecimalFormat("#.00")
                val imcResp = "IMC: " + df.format(imc) + "\n" + checkIMC(imc)
                tvResp.text = imcResp


                    if (imcResp < 18.toString()){
                        imageView.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.bajo))
                        if (imcResp < 24.9.toString()){
                            imageView.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.normal))
                            if (imcResp < 29.9.toString()){
                                imageView.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.sobrepeso))

                            }
                else{
                                imageView.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.obeso))
                            }

                        }

                    }

            }
            else{
                tvResp.text = "Valores nulos."
            }
            it.hideKeyboard()
        }
    }

    private fun calcIMC(peso: String, altura: String): Double  = peso.toDouble() / (altura.toDouble() * altura.toDouble())
    private fun checkIMC(db: Double): String{
        return when(db) {
            in 0.0..18.4 -> "Bajo peso."
            in 18.5..24.99 -> "Peso normal."
            in 25.0..29.99 ->  "Sobrepeso."
            else -> "Obesidad"
        }
    }
    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }
}